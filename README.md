# hjacobs.codeberg.page

Codeberg allows you to host a static website ("Codeberg Pages").
This is mostly an example site with not much content.

Example: https://hjacobs.codeberg.page

Docs: https://docs.codeberg.org/codeberg-pages/

The HTML file uses minimal CSS, see also [58 bytes of css to look great nearly everywhere](https://jrl.ninja/etc/1/).
